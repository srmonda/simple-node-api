FROM node:20.11-slim

COPY . .

CMD ["npm", "server"]
